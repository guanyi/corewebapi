﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CoreWebapi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public string Get()
        {
            string messageSent = "enctrpted message";
            AesCryptoServiceProvider cryptProvider = new AesCryptoServiceProvider();
            cryptProvider.Key = Encoding.ASCII.GetBytes("abcdefghabcdefghabcdefghabcdefgh");
            cryptProvider.KeySize = 256;
            cryptProvider.BlockSize = 128;
            //cryptProvider.GenerateKey();
            cryptProvider.Mode = CipherMode.CBC;

            var transform = cryptProvider.CreateEncryptor();
            var encryptedBytes = transform.TransformFinalBlock(Encoding.ASCII.GetBytes("enctrpted message"), 0, messageSent.Length);

            string s = Convert.ToBase64String(encryptedBytes);
            return Convert.ToBase64String(encryptedBytes);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
