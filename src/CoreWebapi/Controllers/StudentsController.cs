﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreWebapi.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CoreWebapi.Controllers
{
    [Route("api/[controller]")]
    public class StudentsController : Controller
    {
        // GET: api/values
        [HttpGet]
        public IEnumerable<Student> Get()
        {
            using (var studentDb = new StudentContext())
            {
                return studentDb.Student.ToList();
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Student Get(int id)
        {
            using (var studentDb = new StudentContext())
            {
                return studentDb.Student.ToList().Find(s=>s.Id==id);
            }
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
